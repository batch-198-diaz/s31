// index.js

/*

	What is a client?

	A client is an application which creates requests for resources from a server. A client will trigger an action, in the web development context, through a URL and wait for the response of the server.

	What is a server?

	A server is able to host and deliver resources requested by a client. In fact, a single server can handle multiple clients.

	What is Node.js?

	Nodejs is a runtime environment which allows us to create/develop backend/server-side applications with Javascript. Because by default, Javascript was conceptualized solely to the front end.
	

*/

console.log("Hello World!");

/*

require() - is a built in JS method which allows us to import packages. Packages are pieces of code we can integrate into our applicatoin.

Modules 
	- are packages we import
	- are objects that contains codes methods or data.

http 
	-is a module that let us create a server which is able to communicate with a client through the use of Hypertext Transfer Protocol.
	- "http" is a default package that comes with NodeJS. It allows us to use methods that let us create servers.

protocol to client-server communicatoin - http://localhost:4000

createServer() method 
	- is a method from the http module that allows us to handle requests and responses from a client and a server respectively.
	- takes a function argument which is able to receive 2 objects. The request object which contains details of the request from the client. The response object which contains details of the response from the server.
	- it ALWAYS receives the request object first before the response.

response.writeHead()
	- is a method of the response object. It allows us to add headers to our response. Headers are additional information about our response. We have 2 arguments in our writeHead() method. The first is the HTTP status code. An HTTP status is just a numerical code to let the client know about the status of their request. 200, means OK, 404 means the resource cannot be found. 'Content-Type' is one of the most recognizable headers. It simply pertains to the data type of our response.

.listen()
	- allows us to assign a port to our server and serve our index.js server in our local machine assigned to port 4000. There are several tasks and processes on our computer/machine that runs on different port numbers.

	hypertext transfer protocol - http://localhost:4000/ - server

	localhost: --- Local Machine : 4000 - current port assigned to server

	4000,4040,8000,5000,3000,4200 - usually used for web dev

	Servers respond differently with different requests and can be differentiated by their endpoints.

	We start our request with our URL. A client can start a diff request with a diff URL. We should be able to respond differently to diff endpoints.

	Information about the URL endpoint of the request is in the request obj.
	Route is the process/way to respond differently to a request.

	/ = url endpoint (default)

	ex. http://localhost:4000/profile - url endpoint is /profile

	request.url contains the URL endpoint

	/ - default endpoint - request URL: http://localhost:4000/
	/favicon.io - browser's default behavior to retrieve the favicon
	
	/profile - request URL - http://localhost:4000/profile
	/favicon.ico - browser's default behavior to retrieve the favicon
*/

let http = require("http");

// console.log(http);

http.createServer(function(request,response){

	// console.log(request.url)

	if(request.url === "/"){
		response.writeHead(200, {'Content-Type': 'text/plain'});
		response.end("Hello from our first Server! This is from / endpoint.");
	} else if(request.url === "/profile"){
		response.writeHead(200, {'Content-Type': 'text/plain'});
		response.end("Hi I'm me!");
	}

    // response.writeHead(200, {'Content-Type': 'text/plain'});
    // response.end("Hello from our first Server!")

}).listen(4000);

console.log("Server is running on localHost:4000!");
