// sample.js


let http = require("http");

// console.log(http);

http.createServer(function(request,response){

    response.writeHead(200, {'Content-Type': 'text/plain'});
    response.end("Hello from our Sample Server!")

}).listen(4040);

console.log("Server is running on localHost:4040!");

